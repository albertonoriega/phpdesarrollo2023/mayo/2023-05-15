<?php
require_once "conexion.inc";

$conexion = conectar('tienda');

$todo = consultaArray($conexion, "SELECT p.codigo codigoProducto, p.nombre nombreProducto, p.precio precioProducto, p.codigo_fabricante codigoFabricante, f.nombre AS 'nombreFabricante' FROM producto p INNER JOIN fabricante f ON f.codigo=p.codigo_fabricante");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    echo "<h2 style='text-align: center'>Listado de productos y fabricantes</h2>";
    gridView($todo);
    ?>

</body>

</html>