<?php
require_once "conexion.inc";

$conexion = conectar('tienda');

$fabricantes = consultaArray($conexion, "select * from fabricante");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    echo "<h2 style='text-align: center'>Listado de Fabricantes</h2>";
    gridView($fabricantes, ['codigo', 'nombre']);
    ?>

</body>

</html>