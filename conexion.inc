<?php


/**
 * Función que nos permite conectar a una base de datos 
 *
 * @param  string $baseDeDatos nombre de la base de datos a la que te vas a conectar
 * @return object Conexión a la base de datos
 */
function conectar($baseDeDatos)
{
    $conexion = new mysqli(
        '127.0.0.1',  // servidor de base de datos
        'root', // usuario
        '', // contraseña
        $baseDeDatos, // nombre de la base de datos
        3306, //puerto
    );
    return $conexion;
}


/**
 * Me permite ejecutar cualquier consulta de sql
 *
 * @param  object $conexion Conexión a la base de datos
 * @param  string $consulta Consulta de SQL a ejecutar
 * @return object objeto de tipo mysqli_result 
 */
function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}

function consultaArray($conexion, $consulta)
{
    $consulta = $conexion->query($consulta);
    return $consulta->fetch_all(MYSQLI_ASSOC);
}

/**
 * Nos devuelve un array con todos los registros de la tabla alumnos
 *
 * @param  object $conexion Conexión a la base de datos
 * @return array Array con todos los fabricantes
 */
function fabricante($conexion)
{
    $ConsultaFabricante = $conexion->query("select * from fabricante");
    return $ConsultaFabricante->fetch_all(MYSQLI_ASSOC);
}

/**
 * Nos devuelve un array con todos los registros de la tabla examenes
 *
 * @param  mixed $conexion Conexión a la base de datos
 * @return array Array con todos los productos
 */
function productos($conexion)
{
    $consultaProductos = $conexion->query("select * from producto");
    return $consultaProductos->fetch_all(MYSQLI_ASSOC);
}

/**
 * Función que te devuelve los registros de las dos tablas
 *
 * @param  object $conexion Conexión a la base de datos
 * @return array $consultafabricantesYproductos Array con todos los registros de las dos tablas
 */
function fabricantesYproductos($conexion)
{
    $consultafabricantesYproductos = $conexion->query("SELECT p.codigo, p.nombre, p.precio, p.codigo_fabricante, f.nombre AS 'nombre_fabricante' FROM producto p INNER JOIN fabricante f ON f.codigo=p.codigo_fabricante");
    return $consultafabricantesYproductos->fetch_all(MYSQLI_ASSOC);
}
