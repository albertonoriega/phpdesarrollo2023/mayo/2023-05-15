<?php
require_once "conexion.inc";

$conexion = conectar('tienda');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    ?>
    <div class="contenido">
        <p>Pincha en fabricante para ver el contenido de todos los fabricantes</p>
        <p>Pincha en productos para ver el contenido de todos los productos</p>
        <p>Pincha en todo para ver el contenido de todos los productos y fabricantes</p>
        <img src="imgs/productoIndex.jpg" alt="">
    </div>
</body>

</html>