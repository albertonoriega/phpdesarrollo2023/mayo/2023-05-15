<?php
require_once 'utilidades.inc';

$pagina = obtenerPagina();


$menu = [
    'Inicio' => 'index.php',
    'Fabricantes' => 'fabricante.php',
    'Productos' => 'productos.php',
    'Todo' => 'todo.php',
];

// para que se apliquen los estilos, el menu tiene que estar dentro de un ul
echo '<nav>';
dibujarMenu($pagina, $menu);
echo '</nav>';
