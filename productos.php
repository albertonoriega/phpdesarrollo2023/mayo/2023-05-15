<?php
require_once "conexion.inc";

$conexion = conectar('tienda');

$productos = consultaArray($conexion, "select * from producto");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <?php
    require_once "_menu.php";
    echo "<h2 style='text-align: center'>Listado de productos</h2>";
    gridView($productos); // como no le pasas los campos, se mostrarán todos los campos
    ?>
    <form action="caros.php">
        <div>
            <label for="numero">Número de productos más caros a mostrar</label>
            <input type="number" name="numero" id="numero">
        </div>
        <button class="caros" name="caros">Mostrar productos más caros</button>

    </form>

</body>

</html>