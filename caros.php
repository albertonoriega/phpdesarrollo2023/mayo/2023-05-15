<?php

require_once 'conexion.inc';

$conexion = conectar('tienda');

if (isset($_GET["caros"])) {

    $numero = $_GET['numero'];


    $resultado = consultaArray($conexion, "SELECT p.codigo CodigoProducto, p.nombre NombreProducto, p.precio PrecioProducto FROM producto p ORDER BY p.precio DESC LIMIT {$numero}");

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="estilos.css">
    </head>

    <body>
    <?php
    require_once "_menu.php";
    echo "<h2 style='text-align: center'>Productos más caros</h2>";
    gridView($resultado);
}
    ?>
    </body>

    </html>